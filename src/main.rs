#![allow(unused_variables, dead_code)]

use std::arch::x86_64::_rdtsc;

#[cfg(target_os = "linux")]
use sdl2::{
    audio::AudioSpecDesired,
    controller::Button,
    controller::{Axis, GameController},
    event::{Event, WindowEvent},
    keyboard::{KeyboardState, Keycode},
    pixels::PixelFormatEnum,
    render::{Canvas, Texture, TextureCreator},
    video::{Window, WindowContext},
    GameControllerSubsystem,
};

const DEFAULT_WIDTH: u32 = 640;
const DEFAULT_HEIGHT: u32 = 480;
const DEFAULT_SAMPLES_PER_SECOND: i32 = 48000;
const BYTES_PER_SAMPLE: u32 = 4;
const PI: f32 = std::f32::consts::PI;

const BYTES_PER_PIXEL: u32 = 4;

const TARGET_QUEUE_SAMPLES: usize = DEFAULT_SAMPLES_PER_SECOND as usize;

#[cfg(target_os = "linux")]
struct GameContext<'a> {
    texture: Texture<'a>,
    canvas: &'a mut Canvas<Window>,
    creator: &'a TextureCreator<WindowContext>,
    w: u32,
    h: u32,
    blue_offset: usize,
    green_offset: usize,
}

fn update_square_wave_buffer(
    buffer: &mut Vec<i16>,
    running_samples_idx: &mut i32,
    mut samples_to_write: usize,
) {
    if samples_to_write % 2 != 0 {
        samples_to_write += 1;
    }
    buffer.resize(samples_to_write, 0);

    let samples_per_second = DEFAULT_SAMPLES_PER_SECOND;
    let tone_hz = 256;
    let tone_vol = 3000;
    let square_wave_period = samples_per_second / tone_hz;
    let half_wave_square_period = square_wave_period / 2;

    let samples_per_channel = samples_to_write / 2;

    let mut sample_out_idx = 0;

    for _ in 0..samples_per_channel {
        let sample_value = if (*running_samples_idx / half_wave_square_period % 2) != 0 {
            tone_vol
        } else {
            -tone_vol
        };

        *running_samples_idx += 1;

        buffer[sample_out_idx] = sample_value;
        buffer[sample_out_idx + 1] = sample_value;
        sample_out_idx += 2;
    }
}

fn create_square_wave_buffer(samples_to_write: usize, running_samples_idx: &mut i32) -> Vec<i16> {
    let mut buffer = vec![0; samples_to_write as usize];
    update_sine_wave_buffer(&mut buffer, samples_to_write, running_samples_idx);
    buffer
}

#[cfg(target_os = "linux")]
fn update_sine_wave_buffer(
    buffer: &mut Vec<i16>,
    mut samples_to_write: usize,
    running_samples_idx: &mut i32,
) {
    if samples_to_write % 2 != 0 {
        samples_to_write += 1;
    }
    buffer.resize(samples_to_write, 0);

    let samples_per_second = DEFAULT_SAMPLES_PER_SECOND;
    let tone_hz = 256;
    let tone_vol = i16::MAX;
    let wave_period = samples_per_second / tone_hz;
    let samples_per_channel = samples_to_write / 2;

    let mut sample_out_idx = 0;

    for _ in 0..samples_per_channel {
        let t = 2.0 * PI * *running_samples_idx as f32 / wave_period as f32;
        let sine_value = t.sin();
        let sample_value = (sine_value * tone_vol as f32) as i16;

        buffer[sample_out_idx] = sample_value;
        buffer[sample_out_idx + 1] = sample_value;
        sample_out_idx += 2;
        *running_samples_idx += 1;
    }
}

#[cfg(target_os = "linux")]
fn create_sine_wave_buffer(samples_to_write: usize, running_samples_idx: &mut i32) -> Vec<i16> {
    let mut buffer = vec![0; samples_to_write as usize];
    update_sine_wave_buffer(&mut buffer, samples_to_write, running_samples_idx);
    buffer
}

fn get_window_dimension(window: Window) -> (u32, u32) {
    window.size()
}

fn get_available_game_controllers(
    controller_subsystem: GameControllerSubsystem,
) -> Vec<GameController> {
    let mut controllers = Vec::new();

    let num_joysticks = controller_subsystem
        .num_joysticks()
        .expect("Can't get number of joysticks");

    for joystick_index in 0..num_joysticks {
        if !controller_subsystem.is_game_controller(joystick_index) {
            continue;
        }

        let controller = controller_subsystem
            .open(joystick_index)
            .expect("Can't get controller");

        controllers.push(controller);
    }

    controllers
}

#[cfg(target_os = "linux")]
fn render_weird_gradient(ctx: &mut GameContext) {
    let w = ctx.w;
    let h = ctx.h;

    ctx.texture
        .with_lock(None, |buffer: &mut [u8], pitch: usize| {
            for y in 0..(h as usize) {
                for x in 0..(w as usize) {
                    let offset = y * pitch + x * BYTES_PER_PIXEL as usize;

                    // ALPHA
                    buffer[offset] = 255;
                    // BLUE
                    buffer[offset + 1] = (x.wrapping_add(ctx.blue_offset)) as u8;
                    // GREEN
                    buffer[offset + 2] = (y.wrapping_add(ctx.green_offset)) as u8;
                    // RED
                    buffer[offset + 3] = 0;
                }
            }
        })
        .unwrap();
}

#[cfg(target_os = "linux")]
fn update_window(ctx: &mut GameContext) {
    ctx.canvas.clear();
    ctx.canvas
        .copy(&ctx.texture, None, None)
        .expect("Can't copy texture");
    ctx.canvas.present();
}

#[cfg(target_os = "linux")]
fn create_texture(creator: &TextureCreator<WindowContext>, w: u32, h: u32) -> Texture {
    creator
        .create_texture_streaming(PixelFormatEnum::RGBA8888, w, h)
        .expect("Can't create texture")
}

fn resize_texture(ctx: &mut GameContext, w: u32, h: u32) {
    ctx.texture = create_texture(ctx.creator, w, h);
    ctx.w = w;
    ctx.h = h;
}

#[cfg(target_os = "linux")]
fn input_handler(
    ctx: &mut GameContext,
    keyboard_state: KeyboardState,
    controllers: &mut Vec<GameController>,
) {
    for i in 0..controllers.len() {
        let controller = &controllers[i];

        if controller.attached() {
            let up = controller.button(Button::DPadUp);
            let down = controller.button(Button::DPadDown);
            let left = controller.button(Button::DPadLeft);
            let right = controller.button(Button::DPadRight);
            let start = controller.button(Button::Start);
            let back = controller.button(Button::Back);
            let left_shoulder = controller.button(Button::LeftShoulder);
            let right_shoulder = controller.button(Button::RightShoulder);
            let a_button = controller.button(Button::A);
            let b_button = controller.button(Button::B);
            let x_button = controller.button(Button::X);
            let y_button = controller.button(Button::Y);

            let stick_x = controller.axis(Axis::LeftX);
            let stick_y = controller.axis(Axis::LeftY);

            if right {}
            if up {}
            if down {}
        } else {
            controllers.remove(i);
        }
    }

    for keycode in keyboard_state
        .pressed_scancodes()
        .filter_map(Keycode::from_scancode)
    {
        match keycode {
            Keycode::Left => ctx.blue_offset = ctx.blue_offset.wrapping_sub(5),
            Keycode::Right => ctx.blue_offset = ctx.blue_offset.wrapping_add(5),
            Keycode::Up => ctx.green_offset = ctx.green_offset.wrapping_sub(5),
            Keycode::Down => ctx.green_offset = ctx.green_offset.wrapping_add(5),
            _ => {}
        }
    }
}

#[cfg(target_os = "linux")]
fn event_handler(e: &Event, ctx: &mut GameContext) -> bool {
    let mut quit = false;

    match e {
        Event::Quit { .. }
        | Event::KeyDown {
            keycode: Some(Keycode::Escape),
            ..
        } => quit = true,
        Event::Window {
            win_event: WindowEvent::Resized(w, h),
            ..
        } => {
            // resize_texture(ctx, w as u32, h as u32);
            update_window(ctx);
        }
        _ => {}
    }

    quit
}

#[cfg(target_os = "linux")]
fn main() {
    let sdl_context = sdl2::init().expect("Can't get sdl context.");
    let video_subsystem = sdl_context.video().expect("Can't init video subsystem.");
    let audio_subsystem = sdl_context.audio().expect("Can't init audio subsystem.");
    let timer_subsystem = sdl_context.timer().expect("Can't init timer subsystem.");
    let controller_subsystem = sdl_context
        .game_controller()
        .expect("Can't init controller subsystem.");

    let window = video_subsystem
        .window("Handmade Hero", DEFAULT_WIDTH as u32, DEFAULT_HEIGHT as u32)
        .resizable()
        .build()
        .unwrap();

    let desired_spec = AudioSpecDesired {
        // divide by 2 because we have two channels
        freq: Some(DEFAULT_SAMPLES_PER_SECOND / 2),
        channels: Some(2),
        samples: None,
    };

    let device = audio_subsystem
        .open_queue::<i16, _>(None, &desired_spec)
        .expect("Can't create a audio queue.");

    let mut canvas = window.into_canvas().build().unwrap();
    let creator = canvas.texture_creator();

    let mut game_context = GameContext {
        canvas: &mut canvas,
        creator: &creator,
        w: DEFAULT_WIDTH,
        h: DEFAULT_HEIGHT,
        texture: create_texture(&creator, DEFAULT_WIDTH, DEFAULT_HEIGHT),
        blue_offset: 0,
        green_offset: 0,
    };

    render_weird_gradient(&mut game_context);
    update_window(&mut game_context);

    let mut controllers = get_available_game_controllers(controller_subsystem);
    let mut event_pump = sdl_context.event_pump().expect("Can't init event pump");
    let mut sound_is_playing: bool = false;
    let mut buffer = Vec::<i16>::new();
    let mut running_sample_idx = 0;

    let count_per_second = timer_subsystem.performance_frequency();
    let mut last_perf_count = timer_subsystem.performance_counter();
    let mut last_cycle_count = unsafe { _rdtsc() };

    'running: loop {
        if !sound_is_playing {
            device.resume();
            buffer = create_sine_wave_buffer(48000, &mut running_sample_idx);
            sound_is_playing = true;
        }

        let audio_samples_size = (device.size() / BYTES_PER_SAMPLE) as usize;

        update_sine_wave_buffer(
            &mut buffer,
            TARGET_QUEUE_SAMPLES - audio_samples_size,
            &mut running_sample_idx,
        );

        device.queue_audio(&buffer[..]).expect("Can't queue audio.");

        if let Some(event) = &event_pump.poll_event() {
            let should_quit = event_handler(event, &mut game_context);

            if should_quit {
                break 'running;
            }
        }

        let keyboard_state = event_pump.keyboard_state();

        input_handler(&mut game_context, keyboard_state, &mut controllers);

        render_weird_gradient(&mut game_context);
        update_window(&mut game_context);

        let end_perf_counter = timer_subsystem.performance_counter();
        let counter_elapsed = end_perf_counter - last_perf_count;
        let ms_per_frame = counter_elapsed as f32 * 1000.0 / count_per_second as f32;
        let fps = count_per_second as f32 / counter_elapsed as f32;

        let end_cycle_count = unsafe { _rdtsc() };
        let cycle_elapsed = (end_cycle_count - last_cycle_count) as f32 / 1_000_000.0;

        println!(
            "{:.2} mspf,  {:.2} fps,  {:.2} mcpf",
            ms_per_frame, fps, cycle_elapsed
        );

        last_perf_count = end_perf_counter;
        last_cycle_count = end_cycle_count;
    }
}
